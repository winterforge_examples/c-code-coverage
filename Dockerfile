FROM ubuntu:24.04
LABEL authors="joel"

RUN apt-get update \
    && apt-get install -y build-essential \
    gcc \
    g++ \
    gdb \
    clang \
    ninja-build \
    cmake \
    libgtest-dev \
    python3-pip

RUN pip install gcovr --break-system-packages
