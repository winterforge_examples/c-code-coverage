#include <gtest/gtest.h>

#include "add.hpp"

TEST(MyLibTests, AddTests) {
    EXPECT_EQ(add(1, 1), 2);
}